package cmsc433.p2;

import java.util.Collections;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;


/**
 * Simulation is the main class used to run the simulation.  You may
 * add any fields (static or instance) or any methods you wish.
 */
public class Simulation {
	// List to track simulation events during simulation
	private static int people;
	private static int c;
	private static int table;
	private static int maxSpace;
	private static boolean r;
	private static int finishedNumber = 0;
	
	
	public static List<SimulationEvent> events; 

	public static List<Customer> tables;

	private static HashMap<Food, Machine> machines;

	public static HashMap<Integer, List<Food>> ordersByOrderNumber;
	public static HashMap<Integer, Object> orderLocks;

	public static LinkedHashSet<Integer> newOrder;
	public static LinkedHashSet<Integer> ordersInProgress;
	public static LinkedHashSet<Integer> ordersFinished;

	private static Thread[] cooks;
	private static Thread[] customers;

	private static Object FinishedLock;
	



	/**
	 * Used by other classes in the simulation to log events
	 * @param event
	 */
	public static void logEvent(SimulationEvent event) {
		events.add(event);
		System.out.println(event);
	}

	/**
	 * 	Function responsible for performing the simulation. Returns a List of 
	 *  SimulationEvent objects, constructed any way you see fit. This List will
	 *  be validated by a call to Validate.validateSimulation. This method is
	 *  called from Simulation.main(). We should be able to test your code by 
	 *  only calling runSimulation.
	 *  
	 *  Parameters:
	 *	@param numCustomers the number of customers wanting to enter Ratsie's
	 *	@param numCooks the number of cooks in the simulation
	 *	@param numTables the number of tables in Ratsie's (i.e. Ratsie's capacity)
	 *	@param machineCapacity the capacity of all machines in Ratsie's
	 *  @param randomOrders a flag say whether or not to give each customer a random order
	 *
	 */
	public static List<SimulationEvent> runSimulation(
			int numCustomers, int numCooks,
			int numTables, 
			int machineCapacity,
			boolean randomOrders
			) {
		
		
		people = numCustomers;
		c = numCooks;
		table = numTables;
		maxSpace = machineCapacity;
		r = randomOrders;
		finishedNumber = 0;
		Object numFinishedLock = new Object();
		
		
		
		

		//This method's signature MUST NOT CHANGE.  


		//We are providing this events list object for you.  
		//  It is the ONLY PLACE where a concurrent collection object is 
		//  allowed to be used.
		events = Collections.synchronizedList(new ArrayList<SimulationEvent>());




		// Start the simulation
		logEvent(SimulationEvent.startSimulation(numCustomers,
				numCooks,
				numTables,
				machineCapacity));
		
		

		// Set things up you might need
		
		tables = new ArrayList<Customer>(numTables);
		machines = new HashMap<Food, Machine>(maxSpace);

		ordersByOrderNumber = new HashMap<Integer, List<Food>>();
		orderLocks = new HashMap<Integer, Object>();

		newOrder = new LinkedHashSet<Integer>();
		ordersInProgress = new LinkedHashSet<Integer>();
		ordersFinished = new LinkedHashSet<Integer>();

		machines.put(FoodType.wings, new Machine(Machine.MachineType.fryer, FoodType.wings, machineCapacity));
		machines.put(FoodType.pizza, new Machine(Machine.MachineType.oven, FoodType.pizza, machineCapacity));
		machines.put(FoodType.sub, new Machine(Machine.MachineType.grillPress, FoodType.sub, machineCapacity));
		machines.put(FoodType.soda, new Machine(Machine.MachineType.fountain, FoodType.soda, machineCapacity));


		// Start up machines



		// Let cooks in
		cooks = new Thread[numCooks];
		for (int i = 0; i < cooks.length; i++) {
			cooks[i] = new Thread(new Cook("Cook "+i, machines));
		}


		// Build the customers.
		customers = new Thread[numCustomers];
		LinkedList<Food> order;
		if (!randomOrders) {
			order = new LinkedList<Food>();
			order.add(FoodType.wings);
			order.add(FoodType.pizza);
			order.add(FoodType.sub);
			order.add(FoodType.soda);
			for(int i = 0; i < customers.length; i++) {
				customers[i] = new Thread(
						new Customer("Customer " + (i+1), order)
						);
			}
		}
		else {
			for(int i = 0; i < customers.length; i++) {
				Random rnd = new Random();
				int wingsCount = rnd.nextInt(4);
				int pizzaCount = rnd.nextInt(4);
				int subCount = rnd.nextInt(4);
				int sodaCount = rnd.nextInt(4);
				order = new LinkedList<Food>();
				for (int b = 0; b < wingsCount; b++) {
					order.add(FoodType.wings);
				}
				for (int f = 0; f < pizzaCount; f++) {
					order.add(FoodType.pizza);
				}
				for (int f = 0; f < subCount; f++) {
					order.add(FoodType.sub);
				}
				for (int c = 0; c < sodaCount; c++) {
					order.add(FoodType.soda);
				}
				customers[i] = new Thread(
						new Customer("Customer " + (i+1), order)
				);
			}
		}
		
		for (int i = 0; i < cooks.length; i++) {
			cooks[i].start();
		}


		// Now "let the customers know the shop is open" by
		//    starting them running in their own thread.
		for(int i = 0; i < customers.length; i++) {
			customers[i].start();
			//NOTE: Starting the customer does NOT mean they get to go
			//      right into the shop.  There has to be a table for
			//      them.  The Customer class' run method has many jobs
			//      to do - one of these is waiting for an available
			//      table...
		}


		try {
			// Wait for customers to finish
			//   -- you need to add some code here...
			for (int i = 0; i < customers.length; i++) {
				customers[i].join();
			}

			while (!allOrdersFinished()) {
				synchronized(FinishedLock){
					try {
						FinishedLock.wait();
					} catch(InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
			
			
			

			// Then send cooks home...
			// The easiest way to do this might be the following, where
			// we interrupt their threads.  There are other approaches
			// though, so you can change this if you want to.
			for(int i = 0; i < cooks.length; i++)
				cooks[i].interrupt();
			for(int i = 0; i < cooks.length; i++)
				cooks[i].join();

		}
		catch(InterruptedException e) {
			System.out.println("Simulation thread interrupted.");
		}//
		

		// Shut down machines
		Simulation.logEvent(SimulationEvent.machineEnding(machines.remove(FoodType.wings)));
		Simulation.logEvent(SimulationEvent.machineEnding(machines.remove(FoodType.pizza)));
		Simulation.logEvent(SimulationEvent.machineEnding(machines.remove(FoodType.sub)));
		Simulation.logEvent(SimulationEvent.machineEnding(machines.remove(FoodType.soda)));
		



		// Done with simulation		
		logEvent(SimulationEvent.endSimulation());

		return events;
	}
	
	public static boolean enter(Customer customer) {
		if (customer == null || tables == null) {
			return false;
		}
		synchronized(tables) {
			while (tables.size() >= table) {
				try {
					tables.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			tables.add(customer);
			return true;
		}
	}
	
	public static boolean leave(Customer customer) {
		if (customer == null || tables == null || !tables.contains(customer)) {
			return false;
		}
		synchronized(tables) {
			tables.remove(customer);
			tables.notify();
			tables.notify();
		}
		return true;
	}
	
	public static boolean submitOrder(Customer customer, List<Food> order, int orderNumber) {

		if (customer == null || order == null || newOrder == null || ordersByOrderNumber == null) {	
			return false;
		}

		synchronized(ordersByOrderNumber) {
			ordersByOrderNumber.put(orderNumber, order);
		}

		synchronized(orderLocks) {
			orderLocks.put(orderNumber, new Object());
		}

		synchronized(newOrder) {
			newOrder.add(orderNumber);
			synchronized(Simulation.class) {
				Simulation.class.notify();
				Simulation.class.notify();
			}
			newOrder.notifyAll();
			return true;
		}
	}
	
	public static Object getLock(int orderNumber) {
		synchronized(orderLocks) {
			return orderLocks.get(orderNumber);
		}
	}
	
	public static void cookStartedFood(Cook cook, Integer orderNumber) {
		synchronized(getLock(orderNumber)) {
			synchronized(ordersInProgress) {
				ordersInProgress.add(orderNumber);
				getLock(orderNumber).notify();
				getLock(orderNumber).notify();
			}
		}
	}
	
	public static void cookCompletedOrder(Cook cook, int orderNumber) {
		synchronized(Simulation.getLock(orderNumber)) {
			synchronized(ordersInProgress) {
				ordersInProgress.remove((Object)orderNumber);

				synchronized(ordersFinished) {
					ordersFinished.add(orderNumber);
				}

				
							
			}
			
			getLock(orderNumber).notifyAll();
			}
		}
	
	public static Integer NextOrder() {
		int orderNumber = -1;
		synchronized(newOrder) {
			while (newOrder.isEmpty() && !allOrdersFinished()) {
				if (allOrdersFinished()) {
					return null;
				}
				try {
					newOrder.wait();
				} catch (InterruptedException e) {

				}
				if (allOrdersFinished()) {
					return null;
				}
			}

			Iterator<Integer> it = newOrder.iterator();

			while (it.hasNext()) {
				orderNumber = it.next();
				break;
			}

			newOrder.remove(orderNumber);
			return orderNumber;
		}
	}
	
	public static boolean orderOpen() {
		synchronized(newOrder) {
			return newOrder.size() > 0;
		}
	}
	
	public static List<Food> getOrder(int orderNumber) {
		synchronized(ordersByOrderNumber) {
			return ordersByOrderNumber.get(orderNumber);
		}
	}
	
	public static boolean allOrdersFinished() {
		
			return ordersFinished.size() == people;
		
	}
	
	public static boolean orderInProgress(int orderNumber) {
		synchronized(getLock(orderNumber)) {
			synchronized(ordersFinished) {
				return !ordersFinished.contains(orderNumber);
			}
		}
	}
	
	
	

	

	/**
	 * Entry point for the simulation.
	 *
	 * @param args the command-line arguments for the simulation.  There
	 * should be exactly four arguments: the first is the number of customers,
	 * the second is the number of cooks, the third is the number of tables
	 * in Ratsie's, and the fourth is the number of items each machine
	 * can make at the same time.  
	 */
	public static void main(String args[]) throws InterruptedException {
		// Parameters to the simulation
		/*
		if (args.length != 4) {
			System.err.println("usage: java Simulation <#customers> <#cooks> <#tables> <capacity> <randomorders");
			System.exit(1);
		}
		int numCustomers = new Integer(args[0]).intValue();
		int numCooks = new Integer(args[1]).intValue();
		int numTables = new Integer(args[2]).intValue();
		int machineCapacity = new Integer(args[3]).intValue();
		boolean randomOrders = new Boolean(args[4]);
		 */
		int numCustomers = 10;
		int numCooks =1;
		int numTables = 5;
		int machineCapacity = 4;
		boolean randomOrders = false;


		// Run the simulation and then 
		//   feed the result into the method to validate simulation.
		System.out.println("Did it work? " + 
				Validate.validateSimulation(
						runSimulation(
								numCustomers, numCooks, 
								numTables, machineCapacity,
								randomOrders
								)
						)
				);
	}

}



