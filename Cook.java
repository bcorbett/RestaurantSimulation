package cmsc433.p2;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Cooks are simulation actors that have at least one field, a name.
 * When running, a cook attempts to retrieve outstanding orders placed
 * by Eaters and process them.
 */
public class Cook implements Runnable {
	private final String name;
	private static final int w = 0;
	private static final int p = 1;
	private static final int s = 2;
	private static final int d = 3;
	private static final int f = 4;
	private HashMap<Food, Machine> m;
	private HashMap<Integer, HashMap<Food, LinkedList<Thread>>> t;

	/**
	 * You can feel free to modify this constructor.  It must
	 * take at least the name, but may take other parameters
	 * if you would find adding them useful. 
	 *
	 * @param: the name of the cook
	 */
	public Cook(String name, HashMap<Food,Machine> m) {
		this.name = name;
		this.m = m;
		this.t = new HashMap<Integer, HashMap<Food, LinkedList<Thread>>>();
	}

	public String toString() {
		return name;
	}
	
	public String getName() {
		return name;
	}

	/**
	 * This method executes as follows.  The cook tries to retrieve
	 * orders placed by Customers.  For each order, a List<Food>, the
	 * cook submits each Food item in the List to an appropriate
	 * Machine, by calling makeFood().  Once all machines have
	 * produced the desired Food, the order is complete, and the Customer
	 * is notified.  The cook can then go to process the next order.
	 * If during its execution the cook is interrupted (i.e., some
	 * other thread calls the interrupt() method on it, which could
	 * raise InterruptedException if the cook is blocking), then it
	 * terminates.
	 */
	public void run() {

		Simulation.logEvent(SimulationEvent.cookStarting(this));
		try {
			while(true) {
				List<Food> order = null;
				Integer orderNum;
				//YOUR CODE GOES HERE...
				synchronized(Simulation.newOrder){
					while(!Simulation.orderOpen()){
						Simulation.newOrder.wait();
					}
					orderNum = Simulation.NextOrder();
				}
				
				order = Simulation.getOrder(orderNum);
				
				cook(order, orderNum);
				
			}
		}
		catch(InterruptedException e) {
			// This code assumes the provided code in the Simulation class
			// that interrupts each cook thread when all customers are done.
			// You might need to change this if you change how things are
			// done in the Simulation class.
			Simulation.logEvent(SimulationEvent.cookEnding(this));
		}
	}
	
	private void cook(List <Food> order, Integer orderNum) throws InterruptedException{
		synchronized(Simulation.getLock(orderNum)){
			Simulation.logEvent(SimulationEvent.cookReceivedOrder(this, order, orderNum));
			t.put(orderNum, new HashMap<Food, LinkedList<Thread>>());
			int[] num = new int[f];
			for (Food food : order) {
				if (food == FoodType.wings) {
					++num[w];
				}else if (food == FoodType.pizza){
					++num[p];
				}else if (food == FoodType.sub){
					++num[s];
				}else if (food == FoodType.soda){
					++num[d];
				}else{
					throw new UnsupportedOperationException("Unknown food type: " + food.name);
				}
			}
			
			for (int i = 0; i < f; i++) {
				Food food = getFood(i);
				t.get(orderNum).put(food, new LinkedList<Thread>());
				for (int j = 0; j < num[i]; j++) {
					Machine machine = m.get(food);
					Simulation.logEvent(SimulationEvent.cookStartedFood(this, food, orderNum));
					Simulation.cookStartedFood(this, orderNum);
					Thread thread = machine.makeFood(food);
					t.get(orderNum).get(food).add(thread);
							
				}
				
			}
			for (Food food : t.get(orderNum).keySet()) {
				for (Thread thread : t.get(orderNum).get(food)) {
					thread.join();
					Simulation.logEvent(SimulationEvent.cookFinishedFood(this, food, orderNum));
				}
			}
			Simulation.logEvent(SimulationEvent.cookCompletedOrder(this, orderNum));
			Simulation.cookCompletedOrder(this, orderNum);
			
		}
	}
	
	private Food getFood(int foodNumber) {
		switch (foodNumber) {
		case w: return FoodType.wings;
		case p: return FoodType.pizza;
		case s: return FoodType.sub;
		case d: return FoodType.soda;
		default: throw new UnsupportedOperationException("Unknown food count: " + foodNumber);
		}
	}
}